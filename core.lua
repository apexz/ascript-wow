-- local frame = CreateFrame("Frame")
-- frame:RegisterEvent("PLAYER_ENTERING_WORLD")
-- frame:RegisterEvent("PLAYER_LOGIN")

message("aScript loaded!")
local aScript = "|CFFFFA500[|CFF7EBFF1aScript|r|CFFFFA500]|r";

DEFAULT_CHAT_FRAME:AddMessage(aScript.." v1.0.3 Loaded for |CFF7EBFF1"..UnitName("player").. "|r of |CFF7EBFF1"..GetRealmName().. ".")
DEFAULT_CHAT_FRAME:AddMessage(aScript.." If you encounter any bugs or issues please open an issue on <|CFFFFA500https://gitlab.com/apexz/ascript/issues|r>.")
DEFAULT_CHAT_FRAME:AddMessage(aScript.." To get an overview of all commands please type /|CFF7EBFF1ashelp|r.");

-- Chat Tweaks
local function SetFontSize()
	local fontFile, _, fontFlags = ChatFrame1:GetFont();
	ChatFrame1:SetFont(fontFile, 13, "OUTLINE");
end
 
local f = CreateFrame("Frame") -- a frame to handle events
f:SetScript("OnEvent", function(self, event, ...) -- Event handler
	SetFontSize() -- Then automatically set the saved font size each login
end)
f:RegisterEvent("PLAYER_ENTERING_WORLD") -- Event to listen for. This event fires when you login after all addons have loaded    
f:RegisterEvent("GROUP_ROSTER_UPDATE") 

-- SlashCmds
SLASH_ASHELP1 = '/ashelp';
		function SlashCmdList.ASHELP(msg, editbox)
			DEFAULT_CHAT_FRAME:AddMessage(aScript.." Commandlist:")
			DEFAULT_CHAT_FRAME:AddMessage("/|CFF7EBFF1sb|r - show bags.")
			DEFAULT_CHAT_FRAME:AddMessage("/|CFF7EBFF1rl|r - reload ui.")
			DEFAULT_CHAT_FRAME:AddMessage("/|CFF7EBFF1asxp|r - shows your current XP.")
			DEFAULT_CHAT_FRAME:AddMessage("/|CFF7EBFF1latency|r - shows the latency bar.")
		end

SLASH_SHOWBAGS1 = '/sb';
		function SlashCmdList.SHOWBAGS(msg, editbox)
			DEFAULT_CHAT_FRAME:AddMessage(aScript.." Show backpack activated.");
			DEFAULT_CHAT_FRAME:AddMessage(aScript.." Don't forget to type in /rl once you are done!");
			MainMenuBarBackpackButton:Show()
			CharacterBag0Slot:Show()
			CharacterBag1Slot:Show()
			CharacterBag2Slot:Show()
			CharacterBag3Slot:Show()
		end

SLASH_SHOWXP1 = '/asxp';
		function SlashCmdList.SHOWXP(msg, editbox)
			local pXP = UnitXP("player")
			local XPMax = UnitXPMax("player")
			local retVal = GetXPExhaustion()
			print(aScript.." Your XP is currently at |CFF7EBFF1"..pXP.."|r/|CFF7EBFF1"..XPMax.."|r ("..floor( (pXP / XPMax)*100 ).."|r%).",1,0,0)
			if GetRestState() == 1 then
				print(aScript.." Rested XP value: |CFF7EBFF1"..retVal)
			end
		end

-- Testing purposes
SLASH_SHOWLATENCY1 = '/latency';
	function SlashCmdList.SHOWLATENCY(msg, editbox)
		local nin, nout, nping = GetNetStats()
		if nping <= 199 then print(aScript.." Your Network latency is: ".."|CFF20C000" .. nping .. "|r ms") end	-- Low ping
		if nping >= 200 then print(aScript.." Your Network latency is: ".."|CFFFE8A0E" .. nping .. "|r ms") end -- Mid ping
		if nping >= 300 then print(aScript.." Your Network latency is: ".."|CFFFF0303" .. nping .. "|r ms") end	-- High ping
		print("Network Down ".."|cffffffff"..nin.."KB/s")
		print("Network Up ".."|cffffffff" .. nout.. "KB/s")
	end


-- PlayerName:Hide()
TargetFrameTextureFramePVPIcon:SetAlpha(0)
PlayerPVPIcon:SetAlpha(0)

-- ActionBar Positioning
MainMenuBar:ClearAllPoints()
MainMenuBar:SetPoint("CENTER",UIParent,"BOTTOM", 255, 36)	-- slide right

MultiBarBottomRight:ClearAllPoints()
MultiBarBottomRight:SetPoint("CENTER",MultiBarBottomLeft,"TOP",0,29)

MainMenuBarLeftEndCap:Hide() -- Hide Left Griffon.
MainMenuBarRightEndCap:Hide() -- Hide Right Griffon.
--BonusActionBarFrame:Hide()	-- Hide warrior actionbar
for i=0,3 do _G["MainMenuBarTexture"..i]:SetAlpha(0)end
for i=0,3 do _G["MainMenuMaxLevelBar"..i]:SetAlpha(0)end

SlidingActionBarTexture0:SetAlpha(0) -- hide pet bar background
SlidingActionBarTexture1:SetAlpha(0)

MainMenuBarBackpackButton:Hide() -- Hide backpack
CharacterBag0Slot:Hide()
CharacterBag1Slot:Hide()
CharacterBag2Slot:Hide()
CharacterBag3Slot:Hide()

MainMenuExpBar:SetAlpha(0) -- Hide experience bar
ReputationWatchBar:SetAlpha(0) -- hide reputation bar
ExhaustionTick:SetAlpha(0) -- hide ExhaustionTick on experience bar

ActionBarUpButton:Hide() -- hide PageUp actionbar button
ActionBarDownButton:Hide() -- hide pageDown actionbar button
MainMenuBarPerformanceBarFrame:Hide() -- hide latency bar
MainMenuBarPageNumber:Hide() -- hide actionbar page number

-- MicroMenu
CharacterMicroButton:Hide() -- Hide MicroMenu
SpellbookMicroButton:Hide() -- Hide MicroMenu
TalentMicroButton:Hide() -- Hide MicroMenu
TalentMicroButton:ClearAllPoints()
TalentMicroButton:HookScript("OnShow",function(self) self:Hide() end)
QuestLogMicroButton:Hide() -- Hide MicroMenu
SocialsMicroButton:Hide() -- Hide MicroMenu
LFGMicroButton:Hide() -- Hide MicroMenu
MainMenuMicroButton:Hide() -- Hide MicroMenu
HelpMicroButton:Hide() -- Hide MicroMenu
KeyRingButton:Hide()
KeyRingButton:HookScript("OnShow",function(self) self:Hide() end)
